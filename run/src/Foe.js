/**
 * @author Pjduvivier, THIRDBRAIN SA (thirdbrain.ch) / https://gitlab.com/pjd_TBSA
 */

import { MovingEntity, GameEntity, Quaternion, AABB, Ray, Vector3 } from '../../../../lib/yuka.module.js';
import { Shotgun } from './Shotgun.js';

import world from './World.js';

const q = new Quaternion();
const aabb = new AABB();
const ray = new Ray();
const intersectionPoint = new Vector3();
const intersectionNormal = new Vector3();
const reflectionVector = new Vector3();

class Foe extends MovingEntity {

	constructor() {

		super();


	}



	update( delta ) {



		super.update( delta );

	}

}


export { Foe };
